<?php

namespace App\Traits;

trait DateConverts
{
    private function convert($time)
    {
        if (is_null($time))
            return null;

        $timestamp = strtotime($time);
        $published = date('d.m.Y', $timestamp);

        if ($published === date('d.m.Y')) {
            return trans('date.today', ['time' => date('H:i', $timestamp)]);
        } elseif ($published === date('d.m.Y', strtotime('-1 day'))) {
            return trans('date.yesterday', ['time' => date('H:i', $timestamp)]);
        } else {
            $formatted = trans('date.later', [
                'time' => date('H:i', $timestamp),
                'date' => date('d F' . (date('Y', $timestamp) === date('Y') ? null : ' Y'), $timestamp)
            ]);

            return strtr($formatted, trans('date.month_declensions'));
        }
    }

    public function getCreatedAtAttribute($value)
    {
        return $this->convert($value);
    }

    public function getUpdatedAtAttribute($value)
    {
        return $this->convert($value);
    }

    public function getDeletedAtAttribute($value)
    {
        return $this->convert($value);
    }
}
