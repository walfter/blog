<?php

namespace App\Traits;

use App\Models\Image;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Facades\Storage;

trait Images
{

    public function images(): MorphMany
    {
        return $this->morphMany(Image::class, 'model')->orderBy('created_at', 'DESC');
    }

    public function image(): HasOne
    {
        $path = $this->no_image ?? Image::DEFAULT;
        $name = basename($path);
        return $this->hasOne(Image::class, 'model_id', 'id')
            ->where('model_type', '=', self::class)
            ->where('default', true)
            ->withDefault([
                'name' => $name,
                'path' => $path,
                'default' => true,
            ]);
    }

    protected static function bootImages()
    {
        self::deleting(function ($model) {
            if ($model->images()->count()) {
                foreach ($model->images AS $image) {
                    if (!Storage::exists($image->path))
                        continue;
                    Storage::delete($image->path);
                }
                $model->images()->delete();
            }
        });
    }
}
