<?php


namespace App\Traits;

use App\Scopes\SortOrderScope;

trait Sortable
{
    protected static function bootCreatedAtOrdered() {
        static::addGlobalScope(new SortOrderScope());
    }
}
