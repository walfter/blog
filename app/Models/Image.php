<?php

namespace App\Models;

use App\Traits\Sortable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Support\Facades\Storage;

class Image extends Model
{
    use HasFactory;
    use Sortable;

    public const DEFAULT = '/img/no-image.png';

    protected $fillable = [
        'name',
        'path',
        'default',
        'sort'
    ];

    protected $hidden = [
        'model_id',
        'model_type',
    ];

    public static function upload($request, &$uploadTo = null, $attr_name = 'image')
    {
        if ($request->hasFile($attr_name)) {
            $files = $request->file($attr_name);
            if (!is_array($files)) {
                $files = [$files];
            }
            $images = [];
            $is_default = true;
            if (!is_null($uploadTo) && $uploadTo->images()->count())
                $is_default = false;
            foreach ($files as $file) {
                $path = $file->store(date('Y/m/d'));
                $image = Image::create([
                    'name' => $file->getClientOriginalName(),
                    'path' => $path,
                    'default' => $is_default,
                ]);
                $images[] = $image;
                $is_default = false;
            }
            if (!is_null($uploadTo)) {
                $uploadTo->images()->saveMany($images);
                $uploadTo->save();
            }
            return $images;
        }
        return [];
    }

    public static function beforeSave(Image $image)
    {
        if ($image->default) {
            $model_id = $image->model_id;
            $model_type = $image->model_type;

            Image::where('model_id', $model_id)->where('model_type', $model_type)->update(['default' => false]);
        }
    }

    public static function boot()
    {
        parent::boot();

        self::updating(function (Image $image) {
            self::beforeSave($image);
        });

        self::creating(function (Image $image) {
            self::beforeSave($image);
        });

        self::saving(function (Image $image) {
            self::beforeSave($image);
        });

        self::deleting(function (Image $image) {
            if ($image->default) {
                $model_id = $image->model_id;
                $model_type = $image->model_type;
                $other_image = Image::where('model_id', $model_id)
                    ->where('model_type', $model_type)
                    ->whereNot('id', $image->id)
                    ->first();
                $other_image->default = true;
                $other_image->save();
            }
            if (Storage::exists($image->path))
                Storage::delete($image->path);
        });
    }

    public function owner(): MorphTo
    {
        return $this->morphTo('model');
    }

}

