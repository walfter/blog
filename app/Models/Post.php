<?php

namespace App\Models;

use App\Traits\DateConverts;
use App\Traits\Images;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use HasFactory;
    use Images;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'description',
        'content',
    ];

    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at',
    ];

    public function getHtmlAttribute()
    {
        return html_entity_decode($this->content);
    }
}
