<?php

namespace App\Helpers;

use Illuminate\Http\JsonResponse;

class JSON
{
    public static function good(array $data, int $status = 200): JsonResponse
    {
        return \response()->json([
            'success' => true,
            'payload' => $data
        ], $status);
    }

    public static function bad(array $data, int $status = 400): JsonResponse
    {
        return \response()->json([
            'success' => false,
            'payload' => $data
        ], $status);
    }
}
