<?php

namespace App\Http\Controllers\API\Posts;

use App\Helpers\JSON;
use App\Http\Controllers\Controller;
use App\Http\Resources\PostResource;
use App\Http\Resources\PostsListResource;
use App\Models\Image;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'show']]);
        $this->middleware('admin', ['except' => ['index', 'show']]);
    }

    public function index()
    {
        $posts = Post::with('image')->paginate();
        return JSON::good($posts->toArray());
    }

    private function save(Request $request, Post $post = null)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required'],
            'description' => ['required'],
            'content' => ['required'],
        ]);
        $validated = $validator->validate();

        if ($validator->fails())
            return JSON::bad(['messages' => $validator->errors()->all()]);

        if (is_null($post))
            $post = new Post();

        $post->fill($validated);
        $post->save();
        $is_new = $post->wasRecentlyCreated;
        Image::upload($request, $post);
        return JSON::good([
            'post' => $post->with(['images', 'image'])->get(),
            'is_new' => $is_new,
        ]);
    }

    public function store(Request $request)
    {
        return $this->save($request);
    }

    public function show(Post $post)
    {
        return JSON::good(['post' => $post->with(['images', 'image'])->get()]);
    }

    public function update(Request $request, Post $post)
    {
        return $this->save($request, $post);
    }

    public function destroy(Post $post)
    {
        if ($post->delete()) {
            return JSON::good(['messages' => 'Post deleted']);
        }
        return JSON::bad(['messages' => ['Something wrong']]);
    }
}
