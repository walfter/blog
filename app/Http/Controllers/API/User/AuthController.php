<?php

namespace App\Http\Controllers\API\User;

use App\Helpers\JSON;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class AuthController extends Controller
{
    public function login(Request $request): JsonResponse
    {
        $credentials = $request->only(['email', 'password']);
        $credentials['active'] = true;

        if (!$token = auth()->attempt($credentials)) {
            return JSON::bad(['messages' => ['Unauthorized']], 401);
        }

        return $this->responseWithToken($token);
    }

    public function logout(): JsonResponse
    {
        auth()->logout();
        return JSON::good(['messages' => ['Successfully logged out']]);
    }

    public function refresh(): JsonResponse
    {
        return $this->responseWithToken(auth()->refresh());
    }

    private function responseWithToken($token): JsonResponse
    {
        return JSON::good([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
        ]);
    }
}
