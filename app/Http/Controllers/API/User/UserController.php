<?php

namespace App\Http\Controllers\API\User;

use App\Helpers\JSON;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\JsonResponse;

class UserController extends Controller
{

    public function index(): JsonResponse
    {
        $users = User::all();
        return JSON::good(['items' => $users]);
    }

    public function save(Request $request, User $user = null): JsonResponse
    {
        $email_rule = 'nullable';

        if (is_null($user)) {
            $user = new User();
            $email_rule = 'required';
        }

        $validator = Validator::make($request->all(), [
            'email' => [$email_rule, 'unique:users,email'. ($user->id ? $user->id.',id' : '')],
            'name' => ['required'],
            'password' => ['nullable'],
        ]);

        if ($validator->fails()) {
            return JSON::bad([
                'messages' => $validator->errors()->all()
            ]);
        }

        $user_data = $validator->validate();

        if ($request->has('password'))
            $user_data['password'] = Hash::make($user_data['password']);

        $user->fill($user_data);

        if ($user->save()) {
            return JSON::good([
                'user' => $user,
                'is_new' => $user->wasRecentlyCreated
            ], $user->wasRecentlyCreated ? 201 : 200);
        }

        return JSON::bad(['messages' => 'Somethings wrong']);
    }

    public function store(Request $request): JsonResponse
    {
        return $this->save($request);
    }

    public function show(User $user): JsonResponse
    {
        return JSON::good(compact('user'));
    }

    public function update(Request $request, User $user): JsonResponse
    {
        return $this->save($request, $user);
    }

    public function destroy(Request $request, User $user): JsonResponse
    {
        if ($request->user()->id == $user->id)
            return JSON::bad(['messages' => ['You can\'t deleted yourself']]);
        if ($user->delete()) {
            return JSON::good([
                'deleted' => true
            ]);
        }
        return JSON::bad(['messages' => ['Not deleted']]);
    }
}
