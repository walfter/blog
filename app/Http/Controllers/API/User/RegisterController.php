<?php

namespace App\Http\Controllers\API\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function register(Request $request): JsonResponse
    {
        return (new UserController())->save($request);
    }

    public function verify(Request $request)
    {

    }
}
