<?php

namespace App\Http\Controllers\API\Images;

use App\Helpers\JSON;
use App\Http\Controllers\Controller;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ImageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'show']]);
        $this->middleware('admin', ['except' => ['index', 'show']]);
    }

    public function index()
    {
        $images = Image::paginate();
        return JSON::good($images->toArray());
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => ['nullable', 'image'],
            'images' => ['nullable', 'array'],
            'images.*' => ['nullable', 'image'],
            'model_type' => ['required_with:model_id', 'nullable', 'string'],
            'model_id' => ['required_with:model_type', 'nullable', 'integer', 'exists:'.$request->get('model_type', 'images').',id'],
        ]);

        if ($validator->fails())
            return JSON::bad(['messages' => $validator->errors()->all()]);

        $model = null;
        if ($request->has('model_type')) {
            $model_type = $request->get('model_type');
            $model = $model_type::findOrFail($request->get('model_id', null));
        }

        $images = Image::upload($request, $model, $request->has('image') ? 'image' : 'images');
        return JSON::good(['data' => $images]);
    }

    public function show(Image $image)
    {
        return JSON::good(['image' => $image]);
    }

    public function update(Request $request, Image $image)
    {
        $validator = Validator::make($request->all(), [
            'image' => ['nullable', 'image'],
            'model_type' => ['required_with:model_id', 'nullable', 'string'],
            'model_id' => ['required_with:model_type', 'nullable', 'integer', 'exists:'.$request->get('model_type', 'images').',id'],
        ]);

        if ($validator->fails())
            return JSON::bad(['messages' => $validator->errors()->all()]);

        $validate = $validator->validate();

        if ($request->hasFile('image')) {
            $path = $request->file('image')->store(date('Y/m/d'));
            unset($validate['image']);
            $validate['path'] = $path;
            $validate['name'] = $request->file('image')->getClientOriginalName();
        }
        $image->fill($validate);
        $image->save();

        return JSON::good(['image' => $image]);
    }

    public function destroy(Image $image)
    {
        if ($image->delete())
            return JSON::good(['messages' => ['Image was deleted']]);
        return JSON::bad(['messages' => 'Something wrong']);
    }
}
