<?php

namespace App\Exceptions;

use App\Helpers\JSON;
use Illuminate\Contracts\Container\Container;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Throwable;

class Handler extends ExceptionHandler
{

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, Throwable $e)
    {
        if ($request->wantsJson() ||
            $request->ajax() ||
            $request->is('api/*')
        ){
            $messages = $e->getMessage();
            $code = 400;
            if ($e instanceof ModelNotFoundException) {
                preg_match('/\[(.*)\]/', $e->getMessage(), $matches);
                $model = explode('\\', $matches[1]);
                $model = end($model);
                $messages = [
                    $model.' not found'
                ];
                $code = 404;
            }
            return JSON::bad([
                'messages' => is_array($messages) ? $messages : [$messages]
            ], $code);
        }
        return parent::render($request, $e);
    }
}
